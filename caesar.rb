def offset_char(char, n) # change character chr to the character with ascii chr+n
  case ascii = char.ord 
  when 65..90
    ((ascii - 65 + n) % 26 + 65).chr
  when 97..122
    ((ascii - 97 + n) % 26 + 97).chr
  else
    char
  end
end

def caesar_cipher(phrase, offset)
  phrase.chars.map {|chr| offset_char(chr, offset)}.join
end

# require 'pry-byebug'; binding.pry

print 'Your phrase to code : '
input = gets.chomp
print 'The key : '
offset = gets.chomp.to_i

puts caesar_cipher(input, offset)
